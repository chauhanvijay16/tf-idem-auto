admin_users:
- user1
- user2
- user3
- user4
- user5
automation: 'True'
clusterName: idem-test
cogs: OPEX
local_tags:
  Automation: '{{ params.get("automation") }}'
  COGS: '{{ params.get("cogs") }}'
  Environment: '{{ params.get("profile") }}'
  KubernetesCluster: '{{ params.get("clusterName") }}'
  Owner: '{{ params.get("owner") }}'
owner: org1
profile: test-dev
region: eu-west-3
singleAz: 'True'
