
aws_iam_policy.xyz-admin:
  aws.iam.policy.present:
  - policy_document: {"Statement": [{"Action": ["xyz:*"], "Effect": "Allow", "Resource":
      "*"}, {"Action": ["iam:PassRole"], "Effect": "Allow", "Resource": ["arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster"]},
      {"Action": ["kms:Create*", "kms:Describe*", "kms:Enable*", "kms:List*", "kms:Put*",
      "kms:Update*", "kms:Revoke*", "kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource",
      "kms:UntagResource", "kms:ScheduleKeyDeletion", "kms:CancelKeyDeletion"], "Effect":
      "Allow", "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"}],
      "Version": "2012-10-17"}
  - default_version_id: v1
  - tags: []
  - name: xyz-{{ params.get("clusterName") }}-admin
  - resource_id: {{ params.get("aws_iam_policy.xyz-admin")}}
  - id: ANPAX2FJ77DCY4DVSXP5E
  - path: /

# ToDo: The attribute 'assume_role_policy_document' has resolved value of 'data' state. Please create a variable with resolved value and use { params.get('variable_name') } instead of resolved value of 'data' state.
aws_iam_role.xyz-admin:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.xyz-admin")}}
  - name: xyz-{{ params.get("clusterName") }}-admin
  - arn: arn:aws:iam::123456789012:role/xyz-idem-test-admin
  - id: AROAX2FJ77DC33OWDECR6
  - path: /
  - max_session_duration: 3600
  - tags: {{ params.get("local_tags") }}
  - assume_role_policy_document: {"Version": "2012-10-17", "Statement": [{"Effect":
      "Allow", "Principal": {"AWS": "${data.aws_caller_identity.current.account_id}"},
      "Action": "sts:AssumeRole", "Condition": {"ForAnyValue:StringEquals": {"aws:username":
      "{{ params.get(\"admin_users\") }}"}}}, {"Effect": "Allow", "Principal": {"Service":
      "xyz.amazonaws.com"}, "Action": "sts:AssumeRole"}]}
