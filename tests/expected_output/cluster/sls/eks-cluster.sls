
aws_eks_cluster.cluster:
  aws.eks.cluster.present:
  - name: {{ params.get("clusterName") }}
  - resource_id: {{ params.get("aws_eks_cluster.cluster")}}
  - role_arn: ${aws.iam.role:aws_iam_role.cluster:arn}
  - arn: arn:aws:xyz:eu-west-3:123456789012:cluster/idem-test
  - status: ACTIVE
  - version: {{ params.get("clusterVersion") }}
  - resources_vpc_config:
      clusterSecurityGroupId: sg-0f9910c81ca733164
      endpointPrivateAccess: false
      endpointPublicAccess: true
      publicAccessCidrs:
      - 0.0.0.0/0
      securityGroupIds:
      - ${aws.ec2.security_group:aws_security_group.cluster:resource_id}
      subnetIds:
      - ${aws.ec2.subnet:aws_subnet.cluster-1:resource_id}
      - ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-1:resource_id}
      - ${aws.ec2.subnet:aws_subnet.cluster-0:resource_id}
      - ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-2:resource_id}
      - ${aws.ec2.subnet:aws_subnet.cluster-2:resource_id}
      - ${aws.ec2.subnet:aws_subnet.xyz_public_subnet-0:resource_id}
      vpcId: ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
  - kubernetes_network_config:
      ipFamily: ipv4
      serviceIpv4Cidr: 172.20.0.0/16
  - logging:
      clusterLogging:
      - enabled: true
        types:
        - api
        - audit
        - authenticator
        - controllerManager
        - scheduler
  - tags: {{ params.get("local_tags_dict") }}
