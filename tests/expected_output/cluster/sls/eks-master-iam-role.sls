
aws_iam_role.cluster:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.cluster")}}
  - name: {{ params.get("clusterName") }}-temp-xyz-cluster
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster
  - id: AROAX2FJ77DC2JM67OZSY
  - path: /
  - max_session_duration: 3600
  - tags: {{ params.get("local_tags") }}
  - assume_role_policy_document: {"Version": "2012-10-17", "Statement": [{"Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}, "Action": "sts:AssumeRole"}]}
