pop>=17.0
boto3>=1.20.24
idem==18.6.1
python-hcl2>=3.0.5
ruamel.yaml==0.17.4
