============
tf-idem-auto
============

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

Description
============

A tool to generate Idem sls files for a terraform managed infrastructure


Set Up
============

The aws configuration is required. Set AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables.

Run following commands::

    pip install -e .
    tf_idem_auto


ROADMAP
===========

`Strategic plan for Automating the process of migration from TF to IDEM <https://confluence.eng.vmware.com/display/SYM/Milestone+7+-+Strategic+plan+for+Automating+the+process+of+migration+from+TF+to+IDEM>`_
